# lanti-apro-nodejs

[https://github.com/gothinkster/koa-knex-realworld-example]

## Installation

```sh
# Init the project
# https://github.com/gothinkster/koa-knex-realworld-example
# https://github.com/Vincit/objection.js/tree/master/examples/koa
$ cd src/koa && yarn run knex init
$ cd ..
$ yarn set version berry # Set version to yarn@2
$ yarn dlx @yarnpkg/pnpify --sdk vscode
$ yarn plugin import interactive-tools
$ yarn install
$ yarn upgrade-interactive
$ yarn set version 2.4.2 # Update yarn version
$ yarn set version latest # Update yarn to latest
# Generating secret keys
$ python -c "import os; print(os.urandom(16).hex())"
$ python -c "import secrets; print(secrets.token_urlsafe(24))"
# Run the app
$ yarn run db:init
$ yarn run start
```

## Seeded users for testing

The following users created for testing:

```sh
Email: admin@domain.com
Password: secret

Email: user@domain.com
Password: secret
```

## Links

```sh
https://github.com/yarnpkg/berry/issues/1427
https://yarnpkg.com/getting-started/install
https://yarnpkg.com/getting-started/qa#which-files-should-be-gitignored
https://yarnpkg.com/getting-started/editor-sdks

# Eslint issues in vscode
https://next.yarnpkg.com/advanced/pnpify#vscode-support
https://github.com/microsoft/vscode-eslint/issues/601#issuecomment-518743322
https://next.yarnpkg.com/getting-started/editor-sdks

https://expressjs.com/en/5x/api.html
https://github.com/expressjs/express/blob/5.0/History.md

https://github.com/koajs/koa
https://github.com/koajs/static
https://github.com/koajs/bodyparser
https://github.com/koajs/userauth
https://github.com/koajs/router
https://github.com/koajs/csrf
https://github.com/koajs/examples

https://github.com/gothinkster/koa-knex-realworld-example
https://github.com/gothinkster/realworld/issues/142

# Passport and Koa
https://mherman.org/blog/user-authentication-with-passport-and-koa/
https://github.com/mjhea0/node-koa-api
https://github.com/rkusa/koa-passport

# Casbin with Knex
https://github.com/casbin/node-casbin
https://casbin.org/docs/en/adapters
https://github.com/knex/casbin-knex-adapter
https://github.com/knex/casbin-knex-adapter/blob/master/scripts/docker-compose.yml
https://casbin.org/docs/en/supported-models

# AccessControl with Knex
https://github.com/onury/accesscontrol
https://www.npmjs.com/package/accesscontrol
https://blog.logrocket.com/using-accesscontrol-for-rbac-and-abac-in-node-js/

# CASL with knex
https://github.com/stalniy/casl/issues/8
https://github.com/stalniy/casl/issues/347
https://github.com/stalniy/casl/commit/8571671da6461a7fb6175aa2c8ec71022940a766
https://github.com/stalniy/casl-examples/tree/master/packages/express-blog
https://casl.js.org/v5/en/cookbook/roles-with-static-permissions
https://github.com/stalniy/casl
https://gist.github.com/stalniy/b710d65ac8a6c15f37a435c910624ef7
https://www.npmjs.com/package/cancan

# koa Sessions with Redis
https://github.com/koajs/generic-session/blob/master/example/app.js
https://github.com/koajs/koa-redis
# Store Koa sessions in Postgres
https://mherman.org/blog/user-authentication-with-passport-and-koa/
https://github.com/koajs/session#external-session-stores
https://github.com/tb01923/koa-mysql-session/blob/master/index.js
https://gitlab.com/arch-mage/koa-session-knex-store/-/blob/master/src/index.ts
https://github.com/EarthlingInteractive/koa-generic-session-knex
# pg-promise demo
https://github.com/vitaly-t/pg-promise-demo/tree/master/JavaScript
```
