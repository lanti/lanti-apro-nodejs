Vagrant.require_version ">= 2.2.9"

vagrant_root = File.dirname(__FILE__)
dirname = File.basename(Dir.getwd)

# required_plugins = %w()
# required_plugins.push("vagrant-vbguest")
# required_plugins.each do |plugin|
# 	need_restart = false
# 	unless Vagrant.has_plugin? plugin
# 		puts "Installing Vagrant plugin: #{plugin}"
# 		system "vagrant plugin install #{plugin}"
# 		need_restart = true
# 	end
# 	exec "vagrant #{ARGV.join(' ')}" if need_restart
# end

ENV["LC_ALL"] = "en_US.UTF-8"

# https://www.rubydoc.info/gems/sys-cpu/0.7.2/Sys/CPU
# TODO: if it's over 100%, then set CAP to 100%
# CAP = 2.1 / 3.59 = 0.58 * 100 = 58%
PROJECT_NAME = dirname
NETWORK = "192\.168\.50"
CAP = "58"

servers = [
 	{
		:hostname => "master",
		:box => "bento/debian-10.9",
		:box_version => "202105.25.0",
		:memory => "512",
		:cpus => "1",
		:cap => 100,
	},
 	{
		:hostname => "node-1",
		:box => "bento/debian-9.1",
		:box_version => "201708.22.0",
		:memory => "2048",
		:cpus => "1",
		:cap => CAP,
	},
]

# https://docs.ansible.com/ansible/latest/user_guide/connection_details.html#host-key-checking
# https://gist.github.com/kikitux/86a0bd7b78dca9b05600264d7543c40d
# https://docs.ansible.com/ansible/latest/user_guide/playbooks_best_practices.html
# https://docs.ansible.com/ansible/latest/user_guide/playbooks_reuse_roles.html
$ansible_install = <<~HEREDOC_ANSIBLE
	apt update
	apt install -y software-properties-common python-apt python3-apt

	# apt-add-repository --yes --update ppa:ansible/ansible
	echo "deb http://ppa.launchpad.net/ansible/ansible/ubuntu trusty main" > /etc/apt/sources.list.d/ansible.list
	apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367
	apt update
	apt install -y ansible

	cat > /home/vagrant/.ansible.cfg <<EOF
	[defaults]
	host_key_checking = False
	inventory = /vagrant/staging.ini
	EOF
HEREDOC_ANSIBLE

$vagrant_key = File.expand_path("insecure_private_key", ENV["VAGRANT_HOME"])

Vagrant.configure("2") do |config|
	config.ssh.insert_key = false
	config.vm.provision "file", source: "#{$vagrant_key}",
		destination: "/home/vagrant/.ssh/id_rsa"
	config.vm.provision "shell", privileged: false,
		inline: "chmod 0600 /home/vagrant/.ssh/id_rsa"

	servers.each_with_index do |opts, i|
		config.vm.define opts[:hostname] do |node|
			# node.vbguest.auto_update = false

			node.vm.hostname = opts[:hostname]
			node.vm.box = opts[:box]
			node.vm.box_version = opts[:box_version]
			node.vm.network "private_network", ip: "#{NETWORK}.#{100+i}"

			node.vm.provider "virtualbox" do |o|
				# o.linked_clone = true
				o.cpus = opts[:cpus]
				o.memory = opts[:memory]
				o.customize ["modifyvm", :id, "--cpuexecutioncap", opts[:cap]]
				o.customize ["modifyvm", :id, "--groups", "/#{PROJECT_NAME}"]
			end

			# Disabling the default /vagrant share
			node.vm.synced_folder ".", "/vagrant", disabled: true

			if opts[:hostname] == "master"
				# Set local timezone
				# https://linuxize.com/post/how-to-set-or-change-timezone-on-debian-9/
				# https://wiki.debian.org/TimeZoneChanges
				# https://stackoverflow.com/questions/33939834/how-to-correct-system-clock-in-vagrant-automatically/62717200#62717200
				node.vm.provision "shell",
					inline: "timedatectl set-timezone Europe/Budapest",
					run: "always"
				node.vm.provision "shell", inline: $ansible_install
				node.vm.synced_folder "./playbooks", "/vagrant", type: "virtualbox"
			end

			node.vm.network "forwarded_port", guest: 80, host: 8080+i, id: "http_#{opts[:hostname]}"
			node.vm.network "forwarded_port", guest: 443, host: 8443+i, id: "https_#{opts[:hostname]}"
			node.vm.post_up_message = "#{opts[:hostname]} successfully created!"
		end
	end
end
