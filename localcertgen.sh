#!/bin/bash

set -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

: ${BASE_DIR:="${DIR}/.certs"}

# mkdir -p ${BASE_DIR}
# cd ${BASE_DIR}

# https://letsencrypt.org/docs/certificates-for-localhost/
#openssl req -x509 -out localhost.crt -keyout localhost.key \
#  -newkey rsa:2048 -nodes -sha256 \
#  -subj '/CN=dev.test' -extensions EXT -config <( \
#   printf "[dn]\nCN=dev.test\n[req]\ndistinguished_name = dn\n[EXT]\nsubjectAltName=DNS:dev.test\nkeyUsage=digitalSignature\nextendedKeyUsage=serverAuth")
# https://www.ssl.com/how-to/create-a-pfx-p12-certificate-file-using-openssl/
#openssl pkcs12 -export -out certificate.pfx -inkey localhost.key -in localhost.crt -password pass:secret

#openssl req -x509 -out "${BASE_DIR}/cert.crt" -keyout "${BASE_DIR}/priv.key" \
#  -newkey rsa:2048 -nodes -sha256 \
#  -subj '/CN=dev.test' -extensions EXT -config <( \
#   printf "[dn]\nCN=dev.test\n[req]\ndistinguished_name = dn\n[EXT]\nsubjectAltName=DNS:dev.test\nkeyUsage=digitalSignature\nextendedKeyUsage=serverAuth")
# https://www.ssl.com/how-to/create-a-pfx-p12-certificate-file-using-openssl/
#openssl pkcs12 -export -out "${BASE_DIR}/browser.pfx" -inkey "${BASE_DIR}/priv.key" -in "${BASE_DIR}/cert.crt" -password pass:secret

# https://certificatetools.com/
#generate the RSA private key
# openssl genpkey -outform PEM -algorithm RSA -pkeyopt rsa_keygen_bits:2048 -out ${BASE_DIR}/priv.key
#Create the CSR (Click csrconfig.txt in the command below to download config)
# openssl req -new -nodes -key ${BASE_DIR}/priv.key -config ${DIR}/csrconfig.txt -out ${BASE_DIR}/cert.crt
# Generate pfx file for firefox
# openssl pkcs12 -export -out ${BASE_DIR}/browser.pfx -inkey ${BASE_DIR}/priv.key -in ${BASE_DIR}/cert.crt -password pass:secret

mkdir -p ${BASE_DIR}
# https://serverfault.com/questions/845766/generating-a-self-signed-cert-with-openssl-that-works-in-chrome-58
openssl req \
    -newkey rsa:2048 \
    -x509 \
    -nodes \
    -keyout "${BASE_DIR}/priv.key" \
    -new \
    -out "${BASE_DIR}/cert.crt" \
    -subj /CN=dev.test \
    -reqexts SAN \
    -extensions SAN \
    -config <(cat /usr/ssl/openssl.cnf \
        <(printf '[SAN]\nsubjectAltName=DNS:dev.test')) \
    -sha256 \
    -days 3650
# https://www.ssl.com/how-to/create-a-pfx-p12-certificate-file-using-openssl/
openssl pkcs12 -export -out "${BASE_DIR}/browser.pfx" -inkey "${BASE_DIR}/priv.key" -in "${BASE_DIR}/cert.crt" -password pass:secret
