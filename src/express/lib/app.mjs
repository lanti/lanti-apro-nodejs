import createError from 'http-errors';
import cookieParser from 'cookie-parser';
import csrf from 'csurf';
import express from 'express';
import flash from 'flash';
import logger from 'morgan';
import nunjucks from 'nunjucks';
import passport from 'passport';
import session from 'express-session';
import store from './session.mjs';
import { nodeEnv, absDir } from './helper.mjs';
import './auth.mjs';

import {
  login,
  api,
  index,
  users,
} from '../routes/index.mjs';

const app = express();

// View engine setup
// app.set('views', absDir('./views'));
// app.set('view engine', 'hbs');

// View engine setup
nunjucks.configure(absDir('./views'), {
  autoescape: true,
  express: app,
});
app.set('view engine', 'njk');

if (nodeEnv === 'development') {
  app.use(logger('dev'));
  app.use('/public', express.static(absDir('./public')));
}
app.use(express.json());
app.use(express.urlencoded({ extended: true })); // body-parser, default: false
app.use(cookieParser()); // not needed since express-session@1.5.0
app.use(csrf({ cookie: true }));
app.use(session(store));
// https://github.com/expressjs/flash/issues/1
// https://gist.github.com/brianmacarthur/a4e3e0093d368aa8e423
app.use(flash());
app.use(passport.initialize()); // Initialize after body-parser
app.use(passport.session());

// Routes
app.use(login);
app.use(api);
app.use(index);
app.use(users);

// Catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});

// Error handler
app.use((error, req, res) => {
  // set locals, only providing error in development
  res.locals.message = error.message;
  res.locals.error = nodeEnv === 'development' ? error : {};
  // Render the error page
  res.status(error.status || 500);
  res.render('error');
});

export default app;
