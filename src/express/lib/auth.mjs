import passport from 'passport';
import { Strategy as LocalStrategy } from 'passport-local';
import db from './db.mjs';
import { checkHash } from './bcrypt.mjs';

const options = {
  usernameField: 'email',
  passwordField: 'password',
};
const message = {
  message: 'Incorrect email or password!',
};

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser(async (id, done) => {
  try {
    const user = await db('users').where('id', id).first();
    return done(null, user);
  } catch (err) {
    return done(err, null);
    // return done(err);
  }
});

passport.use(new LocalStrategy(options, async (email, password, done) => {
  try {
    const user = await db('users').where('email', email).first();
    if (!user) return done(null, false, { message: 'Incorrect email' });
    const hash = await checkHash(password, user.hashedPassword);
    if (!hash) return done(null, false, { message: 'Incorrect password' });
    return done(null, user);
  } catch (err) {
    return done(err);
  }
}));
