/*
  eslint-disable
    no-shadow,
    prefer-arrow-callback,
    func-names
*/

import bcrypt from 'bcryptjs';
// import { err } from './helper.mjs';

// Faster: https://www.npmjs.com/package/bcrypt
// Better: https://www.npmjs.com/package/bcryptjs

// function hashString(string) {
//   return bcrypt.genSalt(10, function (err, salt) {
//     if (err) throw err;
//     return bcrypt.hash(string, salt, function (err, hash) {
//       if (err) throw err;
//       return hash;
//     });
//   });
// }

// function checkString(string, hash) {
//   return bcrypt.compare(string, hash, function (err, res) {
//     if (err) throw err;
//     return res;
//   });
// }

// /**
//  * Asynchronously generates a hash for the given string.
//  * @param string Data to hash
//  * @returns {Promise}
//  */
// async function hashString(string) {
//   try {
//     const salt = await bcrypt.genSalt(10);
//     const hash = await bcrypt.hash(string, salt);
//     return hash;
//   } catch (error) {
//     err(error);
//     throw error;
//   }
// }

// /**
//  * Asynchronously compares the given data against the given hash.
//  * @param string Data to compare
//  * @param hash Data to be compared to
//  * @returns {Promise}
//  */
// async function checkHash(string, hash) {
//   try {
//     const compare = await bcrypt.compare(string, hash);
//     return compare;
//   } catch (error) {
//     err(error);
//     throw error;
//   }
// }

/**
 * Asynchronously generates a hash for the given string.
 * @param string Data to hash
 * @returns {Promise}
 */
async function hashString(string) {
  const salt = await bcrypt.genSalt(10);
  const hash = await bcrypt.hash(string, salt);
  return hash;
}

/**
 * Asynchronously compares the given data against the given hash.
 * @param string Data to compare
 * @param hash Data to be compared to
 * @returns {Promise}
 */
async function checkHash(string, hash) {
  const compare = await bcrypt.compare(string, hash);
  return compare;
}

export {
  hashString,
  checkHash,
};
