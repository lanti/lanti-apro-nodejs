import { dirname, resolve } from 'path';
import { fileURLToPath } from 'url';
import debug from 'debug';
import dotenv from 'dotenv';

dotenv.config();

const debugPrefix = 'app';
const info = debug(`${debugPrefix}:info`);
const err = debug(`${debugPrefix}:error`);

const nodeEnv = process.env.NODE_ENV;
const nodePort = process.env.PORT;
const nodeSecret1 = process.env.SECRET_1;
const nodeSecret2 = process.env.SECRET_2;

const dir = dirname(fileURLToPath(import.meta.url));
const absDir = (path) => resolve(dir, '../', path);

// https://www.postgresql.org/docs/current/libpq-connect.html#LIBPQ-CONNSTRING
const dbName = `${process.env.PROJECT_NAME}_${nodeEnv}`;
const connString = `postgresql://${process.env.PGUSER}:${process.env.PGPASSWORD}@${process.env.PGHOST}:${process.env.PGPORT}/${dbName}`;

export {
  info,
  err,
  nodeEnv,
  nodePort,
  nodeSecret1,
  nodeSecret2,
  absDir,
  dbName,
  connString,
};
