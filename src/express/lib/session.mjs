// https://github.com/expressjs/session
// https://github.com/ggcode1/connect-session-knex/blob/master/examples/example-postgres.js

import session from 'express-session';
import KnexSessionStore from 'connect-session-knex';
import db from './db.mjs';
import { nodeEnv, nodeSecret1 } from './helper.mjs';

const KnexStore = KnexSessionStore(session);

const store = new KnexStore({
  knex: db,
  tablename: 'sessions', // optional. Defaults to 'sessions'
});

export default nodeEnv === 'production'
  ? {
    secret: nodeSecret1,
    cookie: {
      maxAge: 10000, // ten seconds, for testing
    },
    store,
  }
  : {
    secret: nodeSecret1,
    resave: false,
    saveUninitialized: false,
  };
