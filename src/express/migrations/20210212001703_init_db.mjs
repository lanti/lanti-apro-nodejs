/* eslint-disable no-multi-str */

// https://stackoverflow.com/questions/52969086/knexjs-raw-query-in-migration
// https://www.postgresql.org/docs/current/datatype-net-types.html
// TODO: alterTable in`usermeta.ip` to `cidr` type

export async function up(knex) {
  await knex.schema
    .createTable('users', (table) => {
      table.increments('id').primary();
      table
        .integer('parentId')
        .unsigned()
        .references('id')
        .inTable('users')
        .onDelete('SET NULL')
        .index();
      table.string('firstName');
      table.string('lastName');
      table.string('username');
      table.string('email');
      table.string('insecurePassword');
      table.string('hashedPassword');
      table.date('dateOfBirth');
      table.jsonb('address');
    })
    .createTable('usermeta', (table) => {
      table.increments('id').primary();
      table
        .integer('userId')
        .unsigned()
        .references('id')
        .inTable('users')
        .onDelete('SET NULL')
        .index();
      table.string('sessionToken', 64);
      table.string('ip', 45);
      table.string('uaBrowser', 255);
      table.timestamp('loginAt', { useTz: true });
      table.timestamp('expirationAt', { useTz: true });
    })
    .createTable('movies', (table) => {
      table.increments('id').primary();
      table.string('name');
    })
    .createTable('vehicles', (table) => {
      table.increments('id').primary();
      table
        .integer('ownerId')
        .unsigned()
        .references('id')
        .inTable('users')
        .onDelete('SET NULL')
        .index();
      table.string('manufacturer');
      table.string('model');
    })
    .createTable('users_movies', (table) => {
      table.increments('id').primary();
      table
        .integer('personId')
        .unsigned()
        .references('id')
        .inTable('users')
        .onDelete('CASCADE')
        .index();
      table
        .integer('movieId')
        .unsigned()
        .references('id')
        .inTable('movies')
        .onDelete('CASCADE')
        .index();
    });
  return knex.schema.raw('\
    ALTER TABLE usermeta \
      ALTER COLUMN ip TYPE cidr \
        USING (trim(ip)::cidr)\
  ');
}

export function down(knex) {
  return knex.schema
    .dropTableIfExists('users_movies')
    .dropTableIfExists('vehicles')
    .dropTableIfExists('movies')
    .dropTableIfExists('usermeta')
    .dropTableIfExists('users');
}
