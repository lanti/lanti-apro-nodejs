import db from '../../lib/db.mjs';
import { err } from '../../lib/helper.mjs';

// $ curl -X GET http://127.0.0.1:3000/api/v1/users
export default async (req, res) => {
  try {
    await db.transaction(async (trx) => {
      const users = await trx('users').select();
      res.json(users);
    });
  } catch (error) {
    err(error); // If we get here, that means that query won't taken place
    res.redirect('/');
  }
};
