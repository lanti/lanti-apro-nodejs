import db from '../../lib/db.mjs';
import { err } from '../../lib/helper.mjs';

// $ curl -X POST http://127.0.0.1:3000/api/v1/users
export default async (req, res) => {
  try {
    await db.transaction(async (trx) => {
      const insertPerson = await trx('users')
        .insert({
          firstName: 'Foo',
          lastName: 'Bar',
          age: 99,
          address: JSON.stringify({
            zip: 9999,
            country: 'Hungary',
            streetName: 'Első',
            streetType: 'utca',
            number: 1,
          }),
        })
        .returning('*');
      res.json(insertPerson);
    });
  } catch (error) {
    err(error); // If we get here, that means that query won't taken place
    res.redirect('/');
  }
};
