import { Router } from 'express';
/* import {
  // csrfProtection,
  flash,
  setFlash,
  // removeFlash,
} from './middlewares.mjs'; */

import getLogin from './login/getLogin.mjs';
import postLogin from './login/postLogin.mjs';
import getLogout from './login/getLogout.mjs';
import getUsers from './api/getUsers.mjs';
import postUsers from './api/postUsers.mjs';
import { getIndex, getString, getJson } from './legacy/index.mjs';
import { getUsersIndex, getUsersBar } from './legacy/users.mjs';

const login = new Router();
const api = new Router();
const index = new Router();
const users = new Router();

// Middleware
/* login
  // .use(csrfProtection)
  .use(flash)
  .use(setFlash); */
login.route('/login')
  .get(getLogin) // GET: /login
  .post(postLogin); // POST: /login
login.get('/logout', getLogout); // GET: /logout

// https://github.com/koajs/router/blob/master/lib/layer.js
// Layer.prototype.setPrefix
const apiPrefix = '/api/v1';
api.route(`${apiPrefix}/users`)
  .get(getUsers) // GET: /api/v1/users
  .post(postUsers); // POST: /api/v1/users

index.get('/', getIndex); // GET: /
index.get('/string', getString); // GET: /string
index.get('/json', getJson); // GET: /json

const usersPrefix = '/users';
users.get(`${usersPrefix}`, getUsersIndex); // GET: /users
users.get(`${usersPrefix}/bar`, getUsersBar); // GET: /users/bar

// login.use(removeFlash); // Middleware

export {
  login,
  api,
  index,
  users,
};
