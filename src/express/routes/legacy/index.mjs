const getIndex = async (req, res) => {
  await res.render('index', {
    title: 'Hello Koa 2!',
  });
};

const getString = async (req, res) => {
  res.send('koa2 string');
};

const getJson = async (req, res) => {
  res.json({
    title: 'koa2 json',
  });
};

export {
  getIndex,
  getString,
  getJson,
};
