const getUsersIndex = (req, res) => {
  res.send('this is a users response!');
};

const getUsersBar = (req, res) => {
  res.send('this is a users/bar response');
};

export {
  getUsersIndex,
  getUsersBar,
};
