export default async (req, res) => {
  await res.render('login/index', {
    csrfToken: req.csrfToken(),
    session: JSON.stringify(req.session, null, 4),
    title: 'Login',
  });
};
