export default async (req, res) => {
  if (req.isAuthenticated()) {
    req.logout();
    res.redirect('/');
  } else {
    res.send({ success: false });
  }
};
