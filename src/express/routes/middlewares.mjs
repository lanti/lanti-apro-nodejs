/* eslint-disable import/prefer-default-export */

// https://github.com/koajs/koa/blob/master/docs/troubleshooting.md
// https://github.com/koajs/koa/blob/master/docs/guide.md
// https://github.com/koajs/koa/blob/master/docs/api/index.md
// https://github.com/koajs/koa/blob/master/docs/error-handling.md
// https://github.com/koajs/koa/wiki

/* import csrf from 'csurf';

// const csrf = async (req, res, next) => {
//   if (!ctx.state.csrf) {
//     ctx.state.csrf = ctx.csrf;
//   }
//   await next();
// };
const csrfProtection = csrf({ cookie: true }); */

const flash = async (req, res, next) => {
  // if (ctx.session.flash) delete ctx.session.flash;
  ctx.flash = (type, msg) => {
    ctx.session.flash = { type, message: msg };
  };
  await next();
};

const setFlash = async (req, res, next) => {
  if (!ctx.state.flash) {
    ctx.state.flash = ctx.flash;
  }
  await next();
};

const removeFlash = async (req, res, next) => {
  if (ctx.session.flash) delete ctx.session.flash;
  await next();
};

export {
  // csrfProtection,
  flash,
  setFlash,
  removeFlash,
};
