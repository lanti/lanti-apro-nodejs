/*
  eslint-disable
    import/no-extraneous-dependencies,
    import/prefer-default-export,
    no-return-assign,
    no-param-reassign,
    no-unused-vars,
    no-await-in-loop
*/

import faker from 'faker';
import moment from 'moment';
import { hashString } from '../lib/bcrypt.mjs';
import { info, err } from '../lib/helper.mjs';

const maxUsers = 250;

// https://github.com/marak/Faker.js/
// http://marak.github.io/faker.js/
// https://rawgit.com/Marak/faker.js/master/examples/browser/index.html
const fakerAge = () => {
  let dob = faker.date.past(50, new Date(2003, 1, 1));
  dob = `${dob.getFullYear()}-${dob.getMonth() + 1}-${dob.getDate()}`; // First month is '1'
  // The difference, in years, between NOW and date-of-birth
  const age = moment().diff(moment(dob, 'YYYY-MM-DD'), 'years');

  return {
    dob,
    age,
  };
};

const createFakeUsers = async (count) => {
  const usersArray = [];
  const streetType = ['árok', 'átjáró', 'dűlő', 'dűlőút', 'erdősor', 'fasor', 'forduló', 'gát', 'határsor', 'határút', 'kapu', 'körönd', 'körtér', 'körút', 'köz', 'lakótelep', 'lejáró', 'lejtő', 'lépcső', 'liget', 'mélyút', 'orom', 'ösvény', 'park', 'part', 'pincesor', 'rakpart', 'sétány', 'sikátor', 'sor', 'sugárút', 'tér', 'udvar', 'út', 'utca', 'üdülőpart'];

  for (let i = 0; i < count; i += 1) {
    const firstName = faker.name.firstName();
    const lastName = faker.name.lastName();
    const username = faker.internet.userName(firstName, lastName);
    const email = faker.internet.exampleEmail(firstName, lastName);
    const password = faker.internet.password(10, true);
    usersArray.push({
      firstName,
      lastName,
      username,
      email,
      insecurePassword: password,
      hashedPassword: await hashString(password),
      dateOfBirth: fakerAge().dob,
      address: JSON.stringify({
        zip: faker.random.number({ min: 1000, max: 9999 }),
        country: faker.address.country(),
        streetName: faker.address.streetName(),
        streetType: faker.random.arrayElement(streetType),
        number: faker.random.number({ min: 1, max: 300 }),
      }),
    });
  }

  return usersArray;
  // await Promise.all(usersArray);
};

const createFakeMovies = (count) => {
  const moviesArray = [];
  for (let i = 0; i < count; i += 1) {
    moviesArray.push({
      name: faker.random.words(),
    });
  }

  return moviesArray;
};

const createFakeVehicles = (count) => {
  const vehiclesArray = [];

  for (let i = 0; i < count; i += 1) {
    vehiclesArray.push({
      manufacturer: faker.vehicle.manufacturer(),
      model: faker.vehicle.model(),
    });
  }

  return vehiclesArray;
};

// https://github.com/knex/knex/issues/251#issuecomment-41062794
// https://stackoverflow.com/questions/49661209/get-response-from-axios-with-await-async
// http://knexjs.org/#Builder-transacting
// http://knexjs.org/#Transactions
// pets.forEach((animal) => animal.ownerId = insertPerson[0]); // Modifies existing object
// pets.map((obj) => ({ ...obj, ownerId: insertPerson[0] })); // Returns new object
export const seed = async (knex) => {
  try {
    await knex.transaction(async (trx) => {
      // await trx('users').insert(createFakeUsers(maxUsers), 'id');
      await trx('users').insert(await createFakeUsers(maxUsers), 'id');

      const insertMovies = await knex('movies')
        .insert(createFakeMovies(100))
        .returning('id')
        .transacting(trx);

      const usersMovies = [];
      insertMovies.forEach((movie, index) => {
        usersMovies.push({
          personId: faker.random.number({ min: 1, max: maxUsers }),
          movieId: faker.random.number({ min: 1, max: 100 }),
        });
      });
      await trx('users_movies').insert(usersMovies);

      const vehicles = createFakeVehicles(100);
      vehicles.map((obj) => obj.ownerId = faker.random.number({ min: 1, max: maxUsers }));
      await trx('vehicles').insert(vehicles);
    });

    await knex.transaction(async (trx) => {
      const users = await trx('users').select('id');
      const parents = faker.random.arrayElements(users, 100);
      const children = users.filter((child) => !parents.includes(child));
      const selectedChildren = faker.random.arrayElements(children, 100);

      await selectedChildren.map(async (child, index) => {
        const updateChildren = await trx('users')
          .where('id', child.id)
          .update({
            parentId: parents[index].id,
          })
          .returning(['id', 'parentId']);
        info(updateChildren);
      });
    });

    await knex.transaction(async (trx) => {
      await trx('usermeta').insert({
        userId: 1,
        sessionToken: '11adf744dcfc54700f1bb75e7a62f988121c2e4d5bd0d894c2cdbff6fdc493df',
        ip: '127.0.0.1',
        uaBrowser: 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:85.0) Gecko/20100101 Firefox/85.0',
        loginAt: '2021-02-14 12:55:12+01',
        expirationAt: '2021-02-28 12:55:12+01',
      });
    });
  } catch (error) {
    err(error); // If we get here, that means that inserts won't taken place
  }
};
