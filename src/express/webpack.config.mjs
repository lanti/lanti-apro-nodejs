/*
  eslint-disable
    import/no-extraneous-dependencies,
*/

import autoprefixer from 'autoprefixer';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import sass from 'sass';
import { /* nodeEnv, */ absDir } from './lib/helper.mjs';

export default {
  mode: 'production',
  entry: [
    absDir('./public/src/js/app.mjs'),
    absDir('./public/src/scss/style.scss'),
  ],
  output: {
    path: absDir('./public/dist'),
    filename: 'bundle.js',
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'bundle.css',
    }),
  ],
  module: {
    rules: [
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          // fallback to style-loader in development
          // {
          //   loader: nodeEnv !== 'production'
          //     ? 'style-loader' // Inject CSS to page
          //     : MiniCssExtractPlugin.loader, // Creates a CSS file per JS file which contains CSS
          // },
          // Inject CSS to page
          // { loader: 'style-loader' },
          { loader: MiniCssExtractPlugin.loader },
          // Translates CSS into CommonJS modules
          { loader: 'css-loader' },
          // Run postcss actions
          {
            loader: 'postcss-loader',
            options: {
              // `postcssOptions` is needed for postcss 8.x;
              // if you use postcss 7.x skip the key
              postcssOptions: {
                // postcss plugins, can be exported to postcss.config.js
                plugins: [autoprefixer],
              },
            },
          },
          // Compiles Sass to CSS
          {
            loader: 'sass-loader',
            options: {
              implementation: sass,
            },
          },
        ],
      },
    ],
  },
};
