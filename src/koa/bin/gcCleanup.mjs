/* eslint-disable import/prefer-default-export */

// https://nodejs.dev/learn/how-to-exit-from-a-nodejs-program
// https://nodejs.org/api/process.html#process_process_exit_code

import db from '../lib/db.mjs';
import { info } from '../lib/helper.mjs';

export const gcCleanup = async () => {
  const timestamp = Math.floor(Date.now() / 1000);
  await db('sessions').where('expires', '<=', timestamp).del();
  info('Session garbage collection triggered at:', timestamp);
  return process.exit();
};

gcCleanup();
