/* eslint-disable object-curly-newline */

// Module dependencies
import { createServer } from 'http';

import app from '../lib/app.mjs';
import { info, err, nodeEnv, nodePort } from '../lib/helper.mjs';

// Create HTTP server
const server = createServer(app.callback());

// Normalize a port into a number, string, or false
function normalizePort(val) {
  const port = parseInt(val, 10);
  if (Number.isNaN(port)) return val; // named pipe
  if (port >= 0) return port; // port number
  return false;
}

// Get port from environment and store in Express
const port = normalizePort(nodePort || '3000');
// app.set('port', port);

// Event listener for HTTP server "error" event
function onError(error) {
  if (error.syscall !== 'listen') throw error;

  const bind = typeof port === 'string'
    ? `Pipe ${port}`
    : `Port ${port}`;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      err(`${bind} requires elevated privileges`);
      process.exit(1);
      break;
    case 'EADDRINUSE':
      err(`${bind} is already in use`);
      process.exit(1);
      break;
    default:
      throw error;
  }
}

// Event listener for HTTP server "listening" event
function onListening() {
  const addr = server.address();
  const bind = typeof addr === 'string'
    ? `pipe ${addr}`
    : `port ${addr.port}`;
  info(`Listening on ${bind}`);
  info(`Environment: ${nodeEnv}`);
}

// Listen on provided port, on all network interfaces
server.listen(port);
server.on('error', onError);
server.on('listening', onListening);
