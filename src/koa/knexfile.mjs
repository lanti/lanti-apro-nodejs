// import { mkdirSync } from 'fs';
import { connString } from './lib/helper.mjs';

// if (process.env.NODE_ENV === 'development') {
//   try {
//     mkdirSync('.data');
//   } catch (err) {
//     if (err.code !== 'EEXIST') throw err;
//   }
// }

export default {
  // development: {
  //   client: 'sqlite3',
  //   connection: {
  //     filename: `./.data/${dbName}.sqlite3`,
  //   },
  //   migrations: {
  //     tableName: 'knex_migrations',
  //     directory: './migrations',
  //     loadExtensions: ['.mjs'],
  //   },
  //   seeds: {
  //     directory: './seeds/dev',
  //     loadExtensions: ['.mjs'],
  //   },
  //   //
  //   useNullAsDefault: true,
  //   pool: {
  //     afterCreate: (conn, cb) => {
  //       conn.run('PRAGMA foreign_keys = ON', cb);
  //     },
  //   },
  // },

  development: {
    client: 'postgresql',
    connection: connString,
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: 'knex_migrations',
      directory: './migrations',
      loadExtensions: ['.mjs'],
    },
    seeds: {
      directory: './seeds',
      loadExtensions: ['.mjs'],
    },
  },

  production: {
    client: 'postgresql',
    connection: connString,
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: 'knex_migrations',
      directory: './migrations',
      loadExtensions: ['.mjs'],
    },
  },
};
