import bodyparser from 'koa-bodyparser';
import cors from '@koa/cors';
import CSRF from 'koa-csrf';
import helmet from 'koa-helmet';
import Koa from 'koa';
import koaMount from 'koa-mount';
import koaStatic from 'koa-static';
import logger from 'koa-logger';
import onerror from 'koa-onerror';
import session from 'koa-session';
import views from 'koa-views';
import flash, { clearFlash } from './flash.mjs';
import passport from './auth.mjs';
// import store, { knexStore } from './session.mjs';
import store from './session.mjs';
// import db from './db.mjs';
// import KnexStore from './KnexStore.mjs';

import {
  err,
  nodeEnv,
  nodeSecret1,
  nodeSecret2,
  absDir,
} from './helper.mjs';

import {
  login,
  api,
  index,
  users,
  sessionTest,
} from '../routes/index.mjs';

const app = new Koa();

onerror(app); // Error handler

app.proxy = true; // Trust proxy
app.keys = [nodeSecret1, nodeSecret2];

// Middlewares
app.use(helmet());
app.use(
  cors({
    origin: '*',
    exposeHeaders: ['Authorization'],
    credentials: true,
    allowMethods: ['GET', 'PUT', 'POST', 'DELETE'],
    allowHeaders: ['Authorization', 'Content-Type'],
    keepHeadersOnError: true,
  }),
);
if (nodeEnv === 'development') {
  app.use(logger());
  app.use(koaMount('/public', koaStatic(absDir('./public'))));
}
app.use(bodyparser({ enableTypes: ['json', 'form'] })); // json/form/text/xml
// app.use(new CSRF({
//   invalidTokenMessage: 'Invalid CSRF token',
//   invalidTokenStatusCode: 403,
//   excludedMethods: ['GET', 'HEAD', 'OPTIONS'],
//   disableQuery: false,
// }));
app.use(views(absDir('./views'), {
  map: { njk: 'nunjucks' },
  extension: 'njk',
}));

app.use(session(store, app)); // Initialize before passport or flash
// app.use(session(knexStore(app), app)); // Initialize before passport or flash
app.use(flash()); // Initialize before passport
app.use(passport.initialize()); // Initialize after bodyparser
app.use(passport.session());
app.use(clearFlash()); // Initialize before routes

// Routes
app.use(login.routes(), login.allowedMethods());
app.use(api.routes(), api.allowedMethods());
app.use(index.routes(), index.allowedMethods());
app.use(users.routes(), users.allowedMethods());
app.use(sessionTest.routes(), sessionTest.allowedMethods());

// Error handling
app.on('error', (error, ctx) => {
  err('server error', error, ctx);
});

export default app;
