import bodyparser from 'koa-bodyparser';
// import CSRF from 'koa-csrf';
import Koa from 'koa';
// import koaMount from 'koa-mount';
// import koaStatic from 'koa-static';
// import logger from 'koa-logger';
// import onerror from 'koa-onerror';
// import passport from 'koa-passport';
// import session from 'koa-session';
// import views from 'koa-views';
import Router from '@koa/router';

// import store from './session.mjs';

/* import {
  // err,
  // nodeEnv,
  nodeSecret1,
  nodeSecret2,
  // absDir,
} from './helper.mjs'; */

/* import {
  // csrf,
  setFlash,
  // removeFlash,
} from '../routes/middlewares.mjs'; */
/* import {
  login,
  api,
  index,
  users,
} from '../routes/index.mjs'; */

const app = new Koa();

// onerror(app); // Error handler

app.proxy = true; // Trust proxy

// Middlewares
/* if (nodeEnv === 'development') {
  app.use(logger());
  app.use(koaMount('/public', koaStatic(absDir('./public'))));
} */
app.use(bodyparser({ enableTypes: ['json', 'form'] })); // json/form/text/xml
/* app.use(new CSRF({
  invalidTokenMessage: 'Invalid CSRF token',
  invalidTokenStatusCode: 403,
  excludedMethods: ['GET', 'HEAD', 'OPTIONS'],
  disableQuery: false,
})); */
/* app.use(views(absDir('./views'), {
  map: { njk: 'nunjucks' },
  extension: 'njk',
})); */

// app.keys = [nodeSecret1, nodeSecret2];
// app.use(session(store, app)); // Initialize before passport
/* app.use(session({
  key: 'koa.sess',
  maxAge: 86400000,
  autoCommit: true,
  overwrite: true,
  httpOnly: true,
  signed: true,
  rolling: false,
  renew: false,
  secure: false,
  sameSite: null,
}, app)); // Initialize before passport */

// Express like flash middleware to Passport
// https://github.com/rkusa/koa-passport/issues/35
// https://github.com/danneu/koa-skeleton/tree/master
/* app.use(async (ctx, next) => {
  // if (ctx.session.flash) delete ctx.session.flash;
  ctx.flash = (type, msg) => {
    ctx.session.flash = { type, message: msg };
  };
  await next();
}); */

app.use(async (ctx, next) => {
  console.log('Before next()');
  await next();
  console.log('After next()');
});

// eslint-disable-next-line import/first, import/newline-after-import
/* import './auth.mjs';
app.use(passport.initialize()); // Initialize after bodyparser
app.use(passport.session()); */

// Clear flash on the next request
/* app.use(async (ctx, next) => {
  // if (ctx.session.flash !== undefined) {
  //   ctx.session.flash = undefined;
  // }
  // console.log('ctx.session.flash: ', ctx.session.flash);

  // ctx.session.flash = this || undefined; // if shorthand
  // await next();
  // if (ctx.session.flash) await next();

  if (ctx.session.flash) delete ctx.session.flash;
  await next();
}); */

// Routes
/* app.use(login.routes(), login.allowedMethods());
// app.use(api.routes(), api.allowedMethods());
// app.use(index.routes(), index.allowedMethods());
// app.use(users.routes(), users.allowedMethods()); */

// /////////////////////////////////////////////////////////////////////////////
// Router test
// /////////////////////////////////////////////////////////////////////////////

const router = new Router();

// router.use(csrf); // Middleware
// router.use(setFlash); // Middleware

router.get('/login', async (ctx) => {
  // await ctx.render('login/index', {
  //   session: JSON.stringify(ctx.session, null, 4),
  //   state: JSON.stringify(ctx.state, null, 4),
  //   // flash: ctx.session.flash,
  //   title: 'Login',
  // });
  ctx.body = {
    // session: JSON.stringify(ctx.session, null, 4),
    // state: JSON.stringify(ctx.state, null, 4),
    // flash: ctx.session.flash,
    title: 'Login',
  };
});

// router.post('/login', passport.authenticate('local', {
//   successRedirect: '/',
//   failureRedirect: '/login',
//   failureFlash: true,
// }));

app.use(router.routes(), router.allowedMethods());

// Error handling
/* app.on('error', (error, ctx) => {
  err('server error', error, ctx);
}); */

export default app;
