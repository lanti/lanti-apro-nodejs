// TODO: passport.authenticationMiddleware()
// https://blog.risingstack.com/node-hero-node-js-authentication-passport-js/

import passport from 'koa-passport';
import { Strategy as LocalStrategy } from 'passport-local';
import db from './db.mjs';
import { checkHash } from './bcrypt.mjs';

const options = {
  usernameField: 'email',
  passwordField: 'password',
  passReqToCallback: true,
};
const message = 'Incorrect email or password!';

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser(async (id, done) => {
  try {
    const user = await db('users').where('id', id).first();
    return done(null, user);
  } catch (err) {
    return done(err, null);
    // return done(err);
  }
});

passport.use(new LocalStrategy(options, async (req, email, password, done) => {
  try {
    const user = await db('users').where('email', email).first();
    if (!user) {
      req.ctx.flash('danger flash-1', message);
      return done(null, false);
    }
    const hash = await checkHash(password, user.password);
    if (!hash) {
      req.ctx.flash('danger flash-2', message);
      return done(null, false);
    }
    return done(null, user);
  } catch (err) {
    return done(err);
  }
}));

export default passport;
