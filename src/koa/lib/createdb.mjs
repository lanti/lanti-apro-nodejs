/* eslint-disable object-curly-newline */

import { exec } from 'child_process';
import { info, err, nodeEnv, dbName } from './helper.mjs';

// https://nodejs.org/api/all.html#child_process_child_process_exec_command_options_callback
if (nodeEnv === 'development') {
  exec(`createdb ${dbName} --template=template0`, (error, stdout, stderr) => {
    if (!error) return info(`"${dbName}" database created!`);
    return err('createdb stderr:', stderr);
  });
}
