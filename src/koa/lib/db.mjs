import knex from 'knex';
import knexConfig from '../knexfile.mjs';
import { nodeEnv } from './helper.mjs';

export default knex(knexConfig[nodeEnv]);
