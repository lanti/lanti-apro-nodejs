/* eslint-disable no-param-reassign */

// https://github.com/expressjs/flash/blob/master/index.js
// https://github.com/jaredhanson/connect-flash/blob/master/lib/flash.js
// https://github.com/jaredhanson/passport/blob/master/lib/middleware/authenticate.js
// https://github.com/jaredhanson/passport-local/blob/master/test/strategy.passreq.test.js
// http://www.passportjs.org/docs/username-password/

// https://stackoverflow.com/questions/64594683/avoid-req-flash-delete-data-on-middleware
// https://sharanvkaur.gitbooks.io/gitbook/content/05-express/express-auth/mongoose/06authorization-flash.html

function push(type = 'info', msg = type) {
  msg = { type, message: msg };
  const ctx = this.ctx || this;
  const messages = ctx.session.flash;
  // Do not allow duplicate flash messages
  for (let i = 0; i < messages.length; i += 1) {
    const message = messages[i];
    if (msg.type === message.type && msg.message === message.message) {
      // messages.splice(i, 1); // Remove already duplicated flash messages
      return this;
    }
  }
  return messages.push(msg);
}

const flash = () => async (ctx, next) => {
  ctx.assert(ctx.session, 401, 'A ctx.session is required!');
  if (!Array.isArray(ctx.session.flash)) ctx.session.flash = [];
  ctx.state.flash = ctx.session.flash;
  ctx.flash = push;
  await next();
};

// Clear flash on the next request
// TODO: ctx.session not get updated in views (feature?)
// https://github.com/expressjs/flash/issues/1
// https://gist.github.com/brianmacarthur/a4e3e0093d368aa8e423
const clearFlash = () => async (ctx, next) => {
  if (Array.isArray(ctx.session.flash)) ctx.session.flash = [];
  await next();
};

const clearMessages = () => async (ctx, next) => {
  if (Array.isArray(ctx.session.messages)) ctx.session.messages = [];
  await next();
};

export {
  flash as default,
  clearFlash,
  clearMessages,
};
