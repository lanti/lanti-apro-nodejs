import { dirname, resolve } from 'path';
import { fileURLToPath } from 'url';
import debug from 'debug';
import 'dotenv/config';
// import dotenv from 'dotenv';
// dotenv.config();

const debugPrefix = 'app';
const info = debug(`${debugPrefix}:info`);
const err = debug(`${debugPrefix}:error`);

const nodeEnv = process.env.NODE_ENV;
const nodePort = process.env.PORT;
const nodeSecret1 = process.env.SECRET_1;
const nodeSecret2 = process.env.SECRET_2;

const dir = dirname(fileURLToPath(import.meta.url));
const absDir = (path) => resolve(dir, '../', path);

// https://www.postgresql.org/docs/current/libpq-connect.html#LIBPQ-CONNSTRING
const dbName = `${process.env.PROJECT_NAME}_${nodeEnv}`;
const connString = `postgresql://${process.env.PGUSER}:${process.env.PGPASSWORD}@${process.env.PGHOST}:${process.env.PGPORT}/${dbName}`;

const currentTime = new Date();
const oneYearFromNow = currentTime.setFullYear(currentTime.getFullYear() + 1);

const sec = 1000;
const min = 60 * sec;
const hour = 60 * min;
const day = 24 * hour;
const month = 30 * day;
const year = 365 * day;

const elapsed = {
  sec,
  min,
  hour,
  day,
  month,
  year,
};

export {
  info,
  err,
  nodeEnv,
  nodePort,
  nodeSecret1,
  nodeSecret2,
  absDir,
  dbName,
  connString,
  oneYearFromNow,
  elapsed,
};
