/* eslint-disable no-console */

// https://github.com/koajs/koa-redis/blob/master/src/index.js
// https://github.com/koajs/session
// https://github.com/EarthlingInteractive/koa-generic-session-knex/blob/master/index.js
// https://github.com/natesilva/koa-generic-session-sequelize/blob/master/index.js
// https://github.com/TMiguelT/koa-pg-session/blob/master/index.js

// Garbage collection
// https://github.com/jmitchell38488/koa-session-local/blob/master/src/index.js

//
// https://github.com/tb01923/koa-mysql-session/blob/master/index.js
// https://github.com/ggcode1/connect-session-knex/tree/master/lib
// https://github.com/koajs/koa-redis/blob/master/src/index.js
//
// https://github.com/tj/connect-redis
// https://github.com/jdesboeufs/connect-mongo
// https://github.com/mweibel/connect-session-sequelize
// https://github.com/chill117/express-mysql-session
// https://github.com/ggcode1/connect-session-knex
// https://github.com/rawberg/connect-sqlite3
// https://github.com/mweibel/connect-session-sequelize

// https://nodejs.org/api/events.html
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes
// https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects/Object_prototypes
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes/static
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_syntax
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/get
// https://eslint.org/docs/rules/constructor-super
// https://eslint.org/docs/rules/prefer-object-spread
// http://knexjs.org/#Schema-hasTable
// https://nodejs.org/api/events.html#events_error_events

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/async_function#description
// https://jakearchibald.com/2017/await-vs-return-vs-return-await/
// https://github.com/eslint/eslint/blob/master/docs/rules/no-return-await.md

// import { strict as assert } from 'assert'; // https://nodejs.org/api/assert.html
import debug from 'debug';
import { elapsed } from './helper.mjs';

const info = debug('KnexStore:info');
const err = debug('KnexStore:err');

class KnexStore {
  constructor(knex, options) {
    this.knex = knex;
    this.options = Object.assign(this.constructor.defaultOpts, options);
    this.dialect = this.constructor.knexDialect(this.knex);

    // https://github.com/koajs/session/blob/master/test/store_with_ctx.js
    // https://github.com/koajs/session/blob/master/test/store_with_ctx.test.js
    this.sessions = {};

    if (this.options.initChecks) {
      this.knexUp()
        .then(() => { if (this.options.gcCleanup) this.dumbsterFire(); })
        .then(() => console.log('this:', this))
        .catch((error) => {
          console.error(error);
          process.exit(1);
        });
    }
  }

  static get defaultOpts() {
    return {
      tableName: 'sessions',
      initChecks: true, // sync: true // if true, Check if table exists & run gc if enabled
      browserSessionLifetime: elapsed.day, // time to remember sessions without TTL (1 day)
      gcCleanup: false, // built-in garbage collection
      gcInterval: 5 * elapsed.sec, // 5 sec for testing
    };
  }

  static knexDialect(knex) {
    let dialect = knex.client.config.client;
    switch (dialect) {
      case 'pg':
      case 'postgres':
        dialect = 'postgresql';
        break;
      case 'mysql2':
      case 'mariasql':
      case 'mariadb':
      case 'maria':
        dialect = 'mysql';
        break;
      case 'sqlite':
        dialect = 'sqlite3';
        break;
      case 'oracledb':
        dialect = 'oracle';
        break;
      default:
        // do nothing
    }
    return dialect;
  }

/*   async get(key, maxAge, options) {
    const timestamp = Math.floor(Date.now() / 1000);
    return this.knex(this.options.tableName)
      .where('id', key)
      .andWhere('expires', '>', timestamp)
      .first()
      .then((rows) => {
        console.log('async get (key):', key);
        console.log('async get (maxAge):', maxAge);
        // console.log('async get (options):', options);
        console.log('async get (rows):', rows);
        if (!rows || rows.length === 0) { return null; }
        return JSON.parse(rows.data);
      })
      .catch((error) => console.error(error));
  }

  async set(key, sess, maxAge, options) {
    if (!maxAge) {
      if (sess.cookie && sess.cookie.maxAge) {
        // standard expiring cookie
        return this.set(key, sess, sess.cookie.maxAge);
      }
      if (this.options.browserSessionLifetime > 0) {
        // browser-session cookie
        return this.set(key, sess, this.options.browserSessionLifetime);
      }
    }

    console.log('async set (key):', key);
    console.log('async set (sess):', sess);
    console.log('async set (maxAge):', maxAge);

    const expires = Math.floor((Date.now() + (Math.max(maxAge, 0) || 0)) / 1000);

    // http://knexjs.org/#Interfaces-Promises
    return this.knex(this.options.tableName)
      .where('id', key)
      .then((rows) => {
        if (rows && rows.length > 0) {
          return this.knex(this.options.tableName)
            .where('id', key)
            .update({
              data: JSON.stringify(sess),
              expires,
            });
        }
        return this.knex(this.options.tableName)
          .insert({
            id: key,
            data: JSON.stringify(sess),
            expires,
          });
      })
      .catch((error) => console.error(error));
  }

  async destroy(key, options) {
    return this.knex(this.options.tableName)
      .where('id', key)
      .del()
      .catch((error) => console.error(error));
  } */

  async get(key, maxAge, options) {
    console.log('get value %j with key %s', this.sessions[key], key);
    options.ctx.state.test = 'get';
    return this.sessions[key];
  }

  async set(key, sess, maxAge, options) {
    console.log('set value %j for key %s', sess, key);
    options.ctx.state.test = 'set';
    this.sessions[key] = sess;
  }

  async destroy(key, options) {
    options.ctx.state.test = 'destroyed';
    this.sessions[key] = undefined;
  }

  async knexUp() {
    return this.knex.schema.hasTable(this.options.tableName).then((exists) => {
      if (!exists) {
        throw new Error(`The table named "${this.options.tableName}" hasn't created yet. Please follow the README how to create one manually and place it in your Knex migrations. Knex session store won't work without it!`);
      }
    });
  }

  // Garbage collection of all sessions in the session table that has expired.
  // This method is turned off by default, use a CRON job instead.
  // Use for development purposes only!
  // https://github.com/TMiguelT/koa-pg-session/blob/master/index.js
  // https://github.com/EarthlingInteractive/koa-generic-session-knex/blob/master/index.js
  async dumbsterFire() {
    const sess = this;
    const { gcInterval } = this.options;
    // Each interval of gcInterval, run the cleanup script
    // Recursive, this way the gcInterval can be dynamic
    return setTimeout(function interval() {
      sess.gc().then(() => setTimeout(interval, gcInterval));
    }, gcInterval);
  }

  // Garbage collection of all sessions in the session table that has expired.
  // Use a CRON job instead, with native Knex call.
  // Use for development purposes only!
  async gc() {
    const timestamp = Math.floor(Date.now() / 1000);
    return this.knex(this.options.tableName)
      .where('expires', '<=', timestamp)
      .del()
      .then(() => console.log('Session garbage collection triggered at:', timestamp))
      .catch((error) => console.error(error));
  }
}

export default KnexStore;
