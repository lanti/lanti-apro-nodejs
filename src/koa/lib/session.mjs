/* eslint-disable max-len */

import RedisStore from 'koa-redis';
import db from './db.mjs';
import KnexStore from './KnexStore.mjs';
import { nodeEnv, elapsed } from './helper.mjs';

// https://github.com/koajs/session
// https://github.com/rkusa/koa-passport
// https://github.com/rkusa/koa-passport-example
// https://github.com/mapmeld/koa-passport-example
// https://github.com/mjhea0/koa-passport-example
// https://github.com/mjhea0/node-koa-api
// https://github.com/7kmCo/koa-example

const sessionConfig = {
  key: 'koa.sess', /** (string) cookie key (default is koa.sess) */
  /** (number || 'session') maxAge in ms (default is 1 days) */
  /** 'session' will result in a cookie that expires when session/browser is closed */
  /** Warning: If a session cookie is stolen, this cookie will never expire */
  maxAge: elapsed.day,
  autoCommit: true, /** (boolean) automatically commit headers (default true) */
  overwrite: true, /** (boolean) can overwrite or not (default true) */
  httpOnly: true, /** (boolean) httpOnly or not (default true) */
  signed: true, /** (boolean) signed or not (default true) */
  rolling: false, /** (boolean) Force a session identifier cookie to be set on every response. The expiration is reset to the original maxAge, resetting the expiration countdown. (default is false) */
  renew: false, /** (boolean) renew session when session is nearly expired, so we can always keep user logged in. (default is false) */
  secure: nodeEnv === 'production', /** (boolean) secure cookie */
  sameSite: null, /** (string) session cookie sameSite options (default null, don't set it) */
};
// or if you prefer all default config, just use => app.use(session(app));

export default nodeEnv === 'production'
  ? { store: new RedisStore() }
  : {
    ...sessionConfig,
    store: new KnexStore(db, { gcCleanup: false }),
  };
