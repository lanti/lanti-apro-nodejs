/* eslint-disable no-multi-str */

// https://github.com/gothinkster/koa-knex-realworld-example/blob/master/src/migrations/20170422213822_init.js

// https://stackoverflow.com/questions/52969086/knexjs-raw-query-in-migration
// https://www.postgresql.org/docs/current/datatype-net-types.html
// TODO: alterTable in`usermeta.ip` to `cidr` type

function knexDialect(knex) {
  let dialect = knex.client.config.client;
  switch (dialect) {
    case 'pg':
    case 'postgres':
      dialect = 'postgresql';
      break;
    case 'mysql2':
    case 'mariasql':
    case 'mariadb':
    case 'maria':
      dialect = 'mysql';
      break;
    case 'sqlite':
      dialect = 'sqlite3';
      break;
    case 'oracledb':
      dialect = 'oracle';
      break;
    default:
      // do nothing
  }
  return dialect;
}

export async function up(knex) {
  // await knex.schema.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";');
  if (knexDialect(knex) === 'postgresql') {
    await knex.schema.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";');
  }
  await knex.schema
    .createTable('users', (table) => {
      // table.increments('id').primary();
      table
        .uuid('id')
        .unique()
        .primary()
        .notNullable()
        .defaultTo(knex.raw('uuid_generate_v4()'));
      table
        .string('email')
        .unique()
        .notNullable();
      table
        .string('username')
        .unique()
        .notNullable();
      table
        .string('password')
        .notNullable();
      table.timestamps(true, true);
      //
      table.string('firstName');
      table.string('lastName');
      table.date('dateOfBirth');
      table.jsonb('address');
      table
        .uuid('parentId')
        .unsigned()
        .references('id')
        .inTable('users')
        .onDelete('SET NULL')
        .index();
    })
    .createTable('sessions', (table) => {
      if (knexDialect(knex) === 'postgresql') {
        table
          .uuid('id')
          .unique()
          .primary()
          .notNullable()
          .defaultTo(knex.raw('uuid_generate_v4()'));
      } else {
        table
          .increments('id')
          .unique()
          .primary()
          .notNullable();
      }
      table.text('data');
      table.bigInteger('expires');
      table.index('expires');
    })
    .createTable('usermeta', (table) => {
      table.increments('id').primary();
      table
        .uuid('userId')
        .unsigned()
        .references('id')
        .inTable('users')
        .onDelete('CASCADE')
        .index();
      table.string('sessionToken', 64);
      table.string('ip', 45);
      table.string('uaBrowser', 255);
      table.timestamp('loginAt', { useTz: true });
      table.timestamp('expirationAt', { useTz: true });
    })
    .createTable('movies', (table) => {
      table.increments('id').primary();
      table.string('name');
    })
    .createTable('vehicles', (table) => {
      table.increments('id').primary();
      table
        .uuid('ownerId')
        .unsigned()
        .references('id')
        .inTable('users')
        .onDelete('CASCADE')
        .index();
      table.string('manufacturer');
      table.string('model');
    })
    .createTable('users_movies', (table) => {
      table.increments('id').primary();
      table
        .uuid('personId')
        .unsigned()
        .references('id')
        .inTable('users')
        .onDelete('CASCADE')
        .index();
      table
        .integer('movieId')
        .unsigned()
        .references('id')
        .inTable('movies')
        .onDelete('CASCADE')
        .index();
    });
  return knex.schema.raw('\
    ALTER TABLE usermeta \
      ALTER COLUMN ip TYPE cidr \
        USING (trim(ip)::cidr)\
  ');
}

export async function down(knex) {
  await knex.schema
    .dropTableIfExists('users_movies')
    .dropTableIfExists('vehicles')
    .dropTableIfExists('movies')
    .dropTableIfExists('usermeta')
    .dropTableIfExists('sessions')
    .dropTableIfExists('users');
  return knex.raw('DROP EXTENSION IF EXISTS "uuid-ossp"');
}
