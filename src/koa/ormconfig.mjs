import { mkdirSync } from 'fs';
import dotenv from 'dotenv';
import { absDir } from './lib/helper.mjs';

dotenv.config();

let database;

if (process.env.NODE_ENV === 'development') {
  try {
    mkdirSync('data');
  } catch (err) {
    if (err.code !== 'EEXIST') throw err;
  }

  database = {
    name: 'default',
    type: 'sqlite',
    database: absDir('./data/typeorm-dev.sqlite'),
    synchronize: true,
  };
} else {
  database = {
    name: 'default',
    type: 'postgres',
    host: process.env.PGHOST || 'localhost',
    port: process.env.PGPORT || 5432,
    username: process.env.PGUSER || 'postgres',
    password: process.env.PGPASSWORD || 'secret',
    database: 'lanti-apro-nodejs' || process.env.PGDATABASE,
    synchronize: true,
  };
}

export default {
  ...database,
  entities: [
    absDir('./typeorm/entity/**/*.mjs'),
  ],
  subscribers: [
    absDir('./typeorm/subscriber/**/*.mjs'),
  ],
  migrations: [
    absDir('./typeorm/migration/**/*.mjs'),
  ],
  cli: {
    entitiesDir: absDir('./typeorm/entity'),
    migrationsDir: absDir('./typeorm/migration'),
    subscribersDir: absDir('./typeorm/subscriber'),
  },
};
