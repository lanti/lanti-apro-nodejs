import Router from '@koa/router';
import {
  csrf,
} from './middlewares.mjs';

import getLogin from './login/getLogin.mjs';
import postLogin from './login/postLogin.mjs';
import getLogout from './login/getLogout.mjs';
import getUsers from './api/getUsers.mjs';
import postUsers from './api/postUsers.mjs';
import { getIndex, getString, getJson } from './legacy/index.mjs';
import { getUsersIndex, getUsersBar } from './legacy/users.mjs';

const login = new Router();
const api = new Router();
const index = new Router();
const users = new Router();
const sessionTest = new Router();

// Middleware
login
  .use(csrf);
login
  .get('/login', getLogin) // GET: /login
  .post('/login', postLogin); // POST: /login
login.get('/logout', getLogout); // GET: /logout

// https://github.com/koajs/router/blob/master/lib/layer.js
// Layer.prototype.setPrefix
const apiPrefix = '/api/v1';
api
  .get(`${apiPrefix}/users`, getUsers) // GET: /api/v1/users
  .post(`${apiPrefix}/users`, postUsers); // POST: /api/v1/users

index.get('/', getIndex); // GET: /
index.get('/string', getString); // GET: /string
index.get('/json', getJson); // GET: /json

const usersPrefix = '/users';
users.get(`${usersPrefix}`, getUsersIndex); // GET: /users
users.get(`${usersPrefix}/bar`, getUsersBar); // GET: /users/bar

// https://github.com/koajs/session/blob/master/test/store_with_ctx.js
// https://github.com/koajs/session/blob/master/test/store_with_ctx.test.js
sessionTest
  .get('/get', async (ctx) => {
    ctx.body = await ctx.state.test;
    console.log('ctx.session /get:', ctx.session);
  })
  .get('/set', async (ctx) => {
    ctx.session = { foo: 'bar' };
    ctx.body = await ctx.state.test;
    console.log('ctx.session /set:', ctx.session);
  })
  .get('/destroy', async (ctx) => {
    console.log('ctx.session /destroy:', ctx.session);
    ctx.session = null;
  });

export {
  login,
  api,
  index,
  users,
  sessionTest,
};
