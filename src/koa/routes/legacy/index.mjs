const getIndex = async (ctx) => {
  await ctx.render('index', {
    title: 'Hello Koa 2!',
    session: JSON.stringify(ctx.session, null, 4),
    state: JSON.stringify(ctx.state, null, 4),
  });
};

const getString = async (ctx) => {
  ctx.body = 'koa2 string';
};

const getJson = async (ctx) => {
  ctx.body = {
    title: 'koa2 json',
  };
};

export {
  getIndex,
  getString,
  getJson,
};
