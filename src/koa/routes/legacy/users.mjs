const getUsersIndex = (ctx) => {
  ctx.body = 'this is a users response!';
};

const getUsersBar = (ctx) => {
  ctx.body = 'this is a users/bar response';
};

export {
  getUsersIndex,
  getUsersBar,
};
