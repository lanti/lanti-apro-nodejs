export default async (ctx) => {
  if (ctx.isAuthenticated()) ctx.flash('warning', 'You already logged in!');

  await ctx.render('login/index', {
    session: JSON.stringify(ctx.session, null, 4),
    state: JSON.stringify(ctx.state, null, 4),
    title: 'Login',
  });
};
