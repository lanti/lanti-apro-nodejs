export default async (ctx) => {
  await ctx.logout();
  ctx.session = null;
  ctx.redirect('/login?logged_in=false');
};
