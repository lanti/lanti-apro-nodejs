import passport from '../../lib/auth.mjs';

/* export default passport.authenticate('local', {
  successRedirect: '/',
  failureRedirect: '/login',
}); */

// www.passportjs.org/docs/username-password/#custom-callback
export default async (ctx, next) => {
  await passport.authenticate('local', (err, user, info) => {
    if (err) return next(err);
    if (!user) return ctx.redirect('/login?logged_in=false');
    ctx.login(user, (err) => {
      if (err) return next(err);
      if (ctx.request.body.rememberme === 'enabled') ctx.session.maxAge = 31556952000;
      ctx.flash('success', 'Login was succesful!');
      return ctx.redirect('/?logged_in=true');
    });
  })(ctx, next);
};
