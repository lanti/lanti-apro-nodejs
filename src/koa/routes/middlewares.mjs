/* eslint-disable import/prefer-default-export */

// https://github.com/koajs/koa/blob/master/docs/troubleshooting.md
// https://github.com/koajs/koa/blob/master/docs/guide.md
// https://github.com/koajs/koa/blob/master/docs/api/index.md
// https://github.com/koajs/koa/blob/master/docs/error-handling.md
// https://github.com/koajs/koa/wiki

const csrf = async (ctx, next) => {
  if (!ctx.state.csrf) {
    ctx.state.csrf = ctx.csrf;
  }
  await next();
};

export {
  csrf,
};
