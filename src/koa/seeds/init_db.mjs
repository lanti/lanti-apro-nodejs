/*
  eslint-disable
    import/no-extraneous-dependencies,
    import/prefer-default-export,
    no-return-assign,
    no-param-reassign,
    no-unused-vars,
    no-await-in-loop
*/

import faker from 'faker';
import moment from 'moment'; // https://momentjs.com/docs/
import { hashString } from '../lib/bcrypt.mjs';
import { err } from '../lib/helper.mjs';

const testUsers = [
  {
    firstName: 'John',
    lastName: 'Adminer',
    username: 'admin',
    email: 'admin@domain.com',
    // insecurePassword: 'secret',
    password: '$2a$10$nbVVx8OMkhO8ufYLxq9lp.DO1QRY60FDcYQQ6MSdCmZs5vLGPb96O',
    dateOfBirth: '1990-01-01',
    address: JSON.stringify({
      zip: 1111,
      country: 'Hungary',
      streetName: 'Petőfi Sándor',
      streetType: 'utca',
      number: 1,
    }),
  },
  {
    firstName: 'John',
    lastName: 'Usera',
    username: 'user',
    email: 'user@domain.com',
    // insecurePassword: 'secret',
    password: '$2a$10$nbVVx8OMkhO8ufYLxq9lp.DO1QRY60FDcYQQ6MSdCmZs5vLGPb96O',
    dateOfBirth: '2000-01-01',
    address: JSON.stringify({
      zip: 1112,
      country: 'Hungary',
      streetName: 'Móricz Zsigmond',
      streetType: 'utca',
      number: 2,
    }),
  },
];

const maxUsers = 250;

// https://github.com/marak/Faker.js/
// http://marak.github.io/faker.js/
// https://rawgit.com/Marak/faker.js/master/examples/browser/index.html
const fakerAge = () => {
  let dob = faker.date.past(50, new Date(2003, 1, 1));
  dob = `${dob.getFullYear()}-${dob.getMonth() + 1}-${dob.getDate()}`; // First month is '1'
  // The difference, in years, between NOW and date-of-birth
  const age = moment().diff(moment(dob, 'YYYY-MM-DD'), 'years');
  return {
    dob,
    age,
  };
};

const createFakeUsers = async (count) => {
  const length = count - testUsers.length;
  const streetType = ['árok', 'átjáró', 'dűlő', 'dűlőút', 'erdősor', 'fasor', 'forduló', 'gát', 'határsor', 'határút', 'kapu', 'körönd', 'körtér', 'körút', 'köz', 'lakótelep', 'lejáró', 'lejtő', 'lépcső', 'liget', 'mélyút', 'orom', 'ösvény', 'park', 'part', 'pincesor', 'rakpart', 'sétány', 'sikátor', 'sor', 'sugárút', 'tér', 'udvar', 'út', 'utca', 'üdülőpart'];

  const usersArray = [];
  testUsers.forEach((user) => usersArray.push(user));

  for (let i = 0; i < length; i += 1) {
    const firstName = faker.name.firstName();
    const lastName = faker.name.lastName();
    const username = faker.internet.userName(firstName, lastName);
    const email = faker.internet.exampleEmail(firstName, lastName);
    const password = faker.internet.password(10, true);
    usersArray.push({
      firstName,
      lastName,
      username,
      email,
      password: await hashString(password),
      dateOfBirth: fakerAge().dob,
      address: JSON.stringify({
        zip: faker.datatype.number({ min: 1000, max: 9999 }),
        country: faker.address.country(),
        streetName: faker.address.streetName(),
        streetType: faker.random.arrayElement(streetType),
        number: faker.datatype.number({ min: 1, max: 300 }),
      }),
    });
  }

  return usersArray;
};

const createFakeMovies = (count) => {
  const moviesArray = [];
  for (let i = 0; i < count; i += 1) {
    moviesArray.push({
      name: faker.random.words(),
    });
  }
  return moviesArray;
};

const createFakeVehicles = (count) => {
  const vehiclesArray = [];
  for (let i = 0; i < count; i += 1) {
    vehiclesArray.push({
      manufacturer: faker.vehicle.manufacturer(),
      model: faker.vehicle.model(),
    });
  }
  return vehiclesArray;
};

// https://github.com/knex/knex/issues/251#issuecomment-41062794
// https://stackoverflow.com/questions/49661209/get-response-from-axios-with-await-async
// http://knexjs.org/#Builder-transacting
// http://knexjs.org/#Transactions
// pets.forEach((animal) => animal.ownerId = insertPerson[0]); // Modifies existing object
// pets.map((obj) => ({ ...obj, ownerId: insertPerson[0] })); // Returns new object
export const seed = async (knex) => {
  try {
    await knex.transaction(async (trx) => {
      await trx('users').insert(await createFakeUsers(maxUsers));
    });

    await knex.transaction(async (trx) => {
      const getUsers = await trx('users').select('id').pluck('id');
      const randomUsers = faker.random.arrayElements(getUsers, 100);

      const insertMovies = await knex('movies')
        .insert(createFakeMovies(100))
        .returning('id')
        .transacting(trx);

      const usersMovies = [];
      insertMovies.forEach((movie, index) => {
        usersMovies.push({
          personId: randomUsers[index],
          movieId: faker.datatype.number({ min: 1, max: 100 }),
        });
      });
      await trx('users_movies').insert(usersMovies);

      const vehicles = createFakeVehicles(100);
      vehicles.map((obj, i) => obj.ownerId = randomUsers[i]);
      await trx('vehicles').insert(vehicles);
    });

    await knex.transaction(async (trx) => {
      const getUsers = await trx('users').select('id').pluck('id');
      const parents = faker.random.arrayElements(getUsers, 100);
      const children = getUsers.filter((child) => !parents.includes(child));
      const selectedChildren = faker.random.arrayElements(children, 100);

      await selectedChildren.map(async (child, index) => {
        const updateChildren = await trx('users')
          .where('id', child)
          .update({
            parentId: parents[index],
          })
          .returning(['id', 'parentId']);
      });
    });

    await knex.transaction(async (trx) => {
      const userId = await trx('users')
        .where('username', testUsers[0].username)
        .first()
        .returning('id');

      await trx('usermeta').insert({
        userId: userId.id,
        sessionToken: '11adf744dcfc54700f1bb75e7a62f988121c2e4d5bd0d894c2cdbff6fdc493df',
        ip: '127.0.0.1',
        uaBrowser: 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:85.0) Gecko/20100101 Firefox/85.0',
        loginAt: '2021-02-14 12:55:12+01',
        expirationAt: '2021-02-28 12:55:12+01',
      });
    });
  } catch (error) {
    err(error); // If we get here, that means that inserts won't taken place
  }
};
